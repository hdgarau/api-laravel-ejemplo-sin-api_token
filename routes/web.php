<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/*
Route::get('/api/{model}','HandlerApiController@index');
Route::get('/api/{model}/{id}','HandlerApiController@list');
*/

/*
Route::get('/api2/users/{id}',function($id){
    return User::find($id);
});
*/
Route::get('/api/test/{model}/{id}',function($model,$id){
    return call_user_func(array('App\\' . $model, 'find'),$id);
});

Route::get('/api/help',function(){
    $path = app_path() . "/";

    $out = [];
    $results = scandir($path);

    foreach ($results as $result) {
        if ($result === '.' || $result === '..' || substr($result,-4) != '.php') continue;
        $classname =  substr('\\App\\' . $result,0,-4);

        if (class_exists( $classname) &&
            method_exists( $classname , 'Routes')
            )
        {
            $func = new  ReflectionMethod ($classname , 'Routes') ;


			$filename = $func->getFileName();
			$start_line = $func->getStartLine() - 1; // it's actually - 1, otherwise you wont get the function() block
			$end_line = $func->getEndLine();
			$length = $end_line - $start_line;

			$source = file($filename);

            $body = implode("", array_slice($source, $start_line, $length));
			preg_match_all ( '/Route::get\(\'(.+?)\',/', $body, $matches);
			$out[substr($classname,5)] = $matches[1];
        }
    }
    return view('help',['models' => $out]);
});

\App\Cuenca::Routes();
\App\Pozo::Routes();