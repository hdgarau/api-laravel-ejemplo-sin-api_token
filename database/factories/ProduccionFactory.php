<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Produccion;
use Faker\Generator as Faker;

$factory->define(Produccion::class, function (Faker $faker) {
    return [
        'anio' => array_rand([2018 => 2018,2019]),
        'mes' => array_rand(range(1,12)) + 1,
        'idempresa' => \App\Empresa::all()->random()->codigodesesco,
        'idpozo' => \App\Pozo::all()->random()->idpozo,
        'idtipopozo' => \App\TipoPozo::all()->random()->idtipopozo, //
        'idtipoestado' => \App\TipoEstado::all()->random()->idtipoestado, //
        'idtipoextraccion' => \App\TipoExtraccion::all()->random()->idtipoextraccion, //
        'prod_pet' => rand(0,1200)/100,
        'prod_gas' => rand(0,1200)/100,
        'prod_agua' => rand(0,1200)/100,
        'iny_agua' => rand(0,1200)/100,
        'iny_gas' => rand(0,1200)/100,
        'iny_co2' => rand(0,1200)/100,
        'iny_otro' => rand(0,1200)/100,
        'tef' => rand(0,1200)/100,
        'vida_util' => rand(0,1200)/100,
        'observaciones' => $faker->optional()->sentence(5),
        'rectificado' => array_rand([true,false]),
    ];
});
