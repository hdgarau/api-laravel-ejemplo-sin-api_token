<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Empresa;
use Faker\Generator as Faker;

$factory->define(Empresa::class, function (Faker $faker) {
    $empresa = $faker->sentence(2);
    $codigodesesco = substr($empresa,0,3);
    $cuit = rand(20011111111,27999999999);
    return [
        'empresa' => $empresa,
        'codigodesesco' => $codigodesesco,
        'cuit' => $cuit,
    ];
});
