<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Yacimiento;
use Faker\Generator as Faker;

$factory->define(Yacimiento::class, function (Faker $faker) {
    $areayacimiento = $faker->sentence(2);
    $codigodesesco = \Illuminate\Support\Str::random(12);
    return [

            'yacimiento' => $areayacimiento,
            'codigodesesco' => $codigodesesco,
            'idestadodearea' => \App\EstadoArea::all()->random()->idestadodearea,
    ];
});
