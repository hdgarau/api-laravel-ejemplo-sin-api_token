<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Pozo;
use Faker\Generator as Faker;

$factory->define(Pozo::class, function (Faker $faker) {
    return [
        'sigla' => \Illuminate\Support\Str::random(15),
        'idestadodearea' => \App\EstadoArea::all()->random()->idestadodearea,
        'idareayacimiento' => \App\Yacimiento::all()->random()->codigodesesco,
        'idcuenca' => \App\Cuenca::all()->random()->codigodesesco,
    ];
});
