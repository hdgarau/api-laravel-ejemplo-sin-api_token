<?php

use App\Cuenca;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    private $_tables = ['areasyacimientoscuencas','produccion','usuarios','pozos','areasyacimientos',
        'ubicaciones','tipospozos','tiposextraccion','tiposestados','estadosdeareas','empresas','cuencas'];
    public function run()
    {
        $this->truncateTables();
        App\Cuenca::insert([
            ['cuenca' => 'Norte', 'codigodesesco' => 'NOR'],
            ['cuenca' => 'Sur', 'codigodesesco' => 'SUR'],
        ]);

        factory(\App\Empresa::class,3)->create();
        App\EstadoArea::insert([
            ['estadodearea' => 'Productiva'], ['estadodearea' => 'Inundada'],
        ]);
        \App\TipoEstado::insert([
           ['tipoestado' => 'abandonado'],['tipoestado' => 'productivo'],
        ]);
        \App\TipoExtraccion::insert([
           ['tipoextraccion' => 'Bombreo'],['tipoextraccion' => 'Presión'],
        ]);
        \App\TipoPozo::insert([
           ['tipopozo' => 'Petrolero'],['tipopozo' => 'De apoyo'],
        ]);
        \App\Ubicacion::insert([
           ['ubicacion' => 'Ubicación 1'],['ubicacion' => 'Ubicción 2'],
        ]);
        factory(\App\Yacimiento::class,10)->create();
        factory(\App\Pozo::class,100)->create();
        factory(\App\Produccion::class,10000)->create();
        for($i=0;$i<100;$i++)
        {
            DB::table('areasyacimientoscuencas')->updateOrInsert(
                [
                    'idcuenca' => Cuenca::all()->random()->idcuenca,
                    'idareayacimiento' => \App\Yacimiento::all()->random()->idareayacimiento
                ],[]
            );
        }
        factory(\App\User::class,3)->create();
        //$this->call(UsersTableSeeder::class);
    }

    private function truncateTables()
    {
        foreach ($this->_tables as $table)
        {
            DB::table($table)->truncate();

        }

    }
}
