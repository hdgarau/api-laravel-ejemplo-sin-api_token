<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePozosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pozos', function (Blueprint $table) {
            $table->bigIncrements('idpozo');
            $table->string('sigla')->unique();
            $table->integer('idestadodearea')->references('idestadodearea')->on('estadosdeareas');
            $table->string('idcuenca')->references('codigodesesco')->on('cuencas');
            $table->string('idareayacimiento')->references('codigodesesco')->on('areasyacimientos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pozos');
    }
}
