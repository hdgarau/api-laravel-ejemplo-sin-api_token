<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProduccionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produccion', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('idempresa')->references('codigodesesco')->on('empresas');
            $table->integer('anio');
            $table->integer('mes');
            $table->integer('idpozo')->references('idpozo')->on('pozos');
            $table->integer('idtipoextraccion')->references('idtipoextraccion')->on('tiposextraccion');
            $table->integer('idtipopozo')->references('idtipopozo')->on('tipospozos');
            $table->integer('idtipoestado')->references('idtipoestado')->on('tiposestados');
            $table->float('prod_pet');
            $table->float('prod_gas');
            $table->float('prod_agua');
            $table->float('iny_agua');
            $table->float('iny_gas');
            $table->float('iny_co2');
            $table->float('iny_otro');
            $table->float('tef');
            $table->float('vida_util');
            $table->string('observaciones')->nullable();
            $table->date('fechaingreso')->default(now());
            $table->boolean('rectificado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produccion');
    }
}
