<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreasyacimientoscuencasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('areasyacimientoscuencas', function (Blueprint $table) {
            $table->bigIncrements('idareayacimientocuenca');
            $table->integer('idareayacimiento')->references('idareayacimiento')->on('areasyacimientos');
            $table->integer('idcuenca')->references('idcuenca')->on('cuencas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('areasyacimientoscuencas');
    }
}
