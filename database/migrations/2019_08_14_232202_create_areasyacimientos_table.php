<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreasyacimientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('areasyacimientos', function (Blueprint $table) {
            $table->bigIncrements('idareayacimiento');
            $table->string('yacimiento');
            $table->string('codigodesesco')->unique();
            $table->integer('idestadodearea')->references('idestadodearea')->on('estadosdeareas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yacimientos');
    }
}
