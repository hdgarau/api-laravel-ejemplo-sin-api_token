<?php
/**
 * Created by PhpStorm.
 * User: HERNAN
 * Date: 14/7/2019
 * Time: 4:49 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class APIParser
{
    protected $objEloquent = null;
    protected $data = [];
    protected $msg = '';
    protected $page = 1;
    protected $limit = 20;

    public function __construct(Model $objEloquent)
    {
        $this->objEloquent = $objEloquent;
    }

    public function response()
    {
        //if($this->objEloquent->cou)
        $this->objEloquent->setPerPage($this->limit);
        return $this->objEloquent ->paginate();
    }
}