<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Ubicacion extends Model
{
    protected $table = 'ubicaciones';
    protected $primaryKey = 'idubicacion';
    const TYPE = 'ubicacion';

    static function getColumnsRelationship(){
        return [
            DB::raw("'" . get_called_class()::TYPE . "' as type"),
            'idubicacion as id',
            'ubicacion as nombre',
        ];
    }
}
