<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TipoPozo extends Model
{
    protected $table = 'tipospozos';
    protected $primaryKey = 'idtipopozo';
    const TYPE = 'tipopozo';

    static function getColumnsRelationship(){
        return [
            DB::raw("'" . get_called_class()::TYPE . "' as type"),
            'idtipopozo as id',
            'tipopozo as nombre',
        ];
    }
}
