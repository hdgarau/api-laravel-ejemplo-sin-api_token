<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EstadoArea extends Model
{
    protected $table = 'estadosdeareas';
    protected $primaryKey = 'idestadodearea';
    const TYPE = 'estado_area';

    static function getColumnsRelationship(){
        return [
            DB::raw("'" . get_called_class()::TYPE . "' as type"),
            'idestadodearea as id',
            'estadodearea as nombre',
            ];
    }
}
