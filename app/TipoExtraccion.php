<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TipoExtraccion extends Model
{
    protected $table = 'tiposextraccion';
    protected $primaryKey = 'idtipoextraccion';
    const TYPE = 'tipoextraccion';

    static function getColumnsRelationship(){
        return [
            DB::raw("'" . get_called_class()::TYPE . "' as type"),
            'idtipoextraccion as id',
            'tipoextraccion as nombre',
        ];
    }
}
