<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TipoEstado extends Model
{
    protected $table = 'tiposestados';
    protected $primaryKey = 'idtipoestado';
    const TYPE = 'tipoestado';

    static function getColumnsRelationship(){
        return [
            DB::raw("'" . get_called_class()::TYPE . "' as type"),
            'idtipoestado as id',
            'tipoestado as nombre',
            ];
    }
}
