<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produccion extends Model
{
    protected $table = 'produccion';
    const TYPE = 'produccion';

    public function Empresa() {
        return $this->hasOne(Empresa::class,'codigodesesco','idempresa')
            ->select(Empresa::getColumnsRelationship());

    }
    public function TipoPozo() {
        return $this->hasOne(TipoPozo::class,'idtipopozo','idtipopozo')
            ->select(TipoPozo::getColumnsRelationship());
    }
    public function TipoEstado() {
        return $this->hasOne(TipoEstado::class,'idtipoestado','idtipoestado')
            ->select(TipoEstado::getColumnsRelationship());
    }
    public function TipoExtraccion() {
        return $this->hasOne(TipoExtraccion::class,'idtipoextraccion','idtipoextraccion')
            ->select(TipoExtraccion::getColumnsRelationship());
    }
}
