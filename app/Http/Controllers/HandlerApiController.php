<?php

namespace App\Http\Controllers;


use App\APIParser;

class HandlerApiController extends Controller
{
    //
    /**
     * @param $model
     * @return mixed
     */
    public function index($model)
    {
        $class = 'App\\'  . rtrim($model,'s');
        $objEloquent = new $class;
        $json = new APIParser($objEloquent);
        return $json->response();
    }
    public function list($model,$id)
    {
        $class = 'App\\'  . rtrim($model,'s');
        $r = new $class;
        return $r->find($id);
    }
}
