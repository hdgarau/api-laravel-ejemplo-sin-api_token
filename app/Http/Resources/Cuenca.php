<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Cuenca extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => \App\Cuenca::TYPE,
            'id' => $this->idcuenca,
            'nombre' => $this->cuenca,
        ];
    }
}
