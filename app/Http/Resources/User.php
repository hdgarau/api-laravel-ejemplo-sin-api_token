<?php

namespace App\Http\Resources;

use App\Empresa;
use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->idinformante,
            'type' => 'usuarios',
            'empresa' => $this->Empresa,
        ];

    }
}
