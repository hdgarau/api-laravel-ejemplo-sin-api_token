<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Produccion extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => \App\Produccion::TYPE,
            'anio' => $this->anio,
            'mes' => $this->mes,
            'empresa' => $this->empresa,
            'tipopozo' => $this->tipopozo, //
            'tipoestado' => $this->tipoestado, //
            'tipoextraccion' => $this->tipoextraccion, //
            'prod_pet' => $this->prod_pet,
            'prod_gas' => $this->prod_gas,
            'prod_agua' => $this->prod_agua,
            'iny_agua' => $this->iny_agua,
            'iny_gas' => $this->iny_gas,
            'iny_co2' => $this->iny_co2,
            'iny_otro' => $this->iny_otro,
            'tef' => $this->tef,
            'vida_util' => $this->vida_util,
            'observaciones' => $this->observaciones,
            'fechaingreso' => $this->fechaingreso,
            'rectificado' => $this->rectificado == 't' ? 'SI' : 'NO',
        ];
    }
}
