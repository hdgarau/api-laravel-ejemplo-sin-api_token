<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Pozo extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => \App\Pozo::TYPE,
            'id' => $this->idpozo,
            'sigla' => $this->sigla,
            'cuenca' => $this->Cuenca,
            'yacimiento' => $this->Yacimiento,
        ];
    }
}
