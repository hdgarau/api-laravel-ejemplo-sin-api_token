<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Empresa extends Model
{
    protected $table = 'empresas';
    protected $primaryKey = 'idempresa';
    const TYPE = 'empresa';

    static function getColumnsRelationship(){
        return [
            DB::raw("'" . get_called_class()::TYPE . "' as type"),
            'idempresa as id',
            'empresa as razonsocial',
            'cuit',
            'codigodesesco'];
    }
    //
    public function usuario(){
        return $this->hasMany(User::class,'idusuario','idusuario');
    }

}
