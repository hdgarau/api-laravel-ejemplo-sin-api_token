<?php

namespace App;

use App\Http\Resources\CuencaCollection;
use App\Http\Resources\YacimientoCollection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class Cuenca extends Model
{
    protected $table = 'cuencas';
    protected $primaryKey = 'idcuenca';
    const TYPE = 'cuenca';

    static function getColumnsRelationship(){
        return [
            DB::raw("'" . get_called_class()::TYPE . "' as type"),
            'idcuenca as id',
            'cuenca as nombre',
            'codigodesesco'];
    }

    public function Yacimientos()
    {
        return $this->belongsToMany('App\Yacimiento','areasyacimientoscuencas','idcuenca', 'idareayacimiento');

    }

    //Rutas
    static function Routes()
    {
        Route::get('/api/cuencas',function(){
            return new CuencaCollection(
                Cuenca::orderBy('idcuenca')->paginate());
        });
        Route::get('/api/cuencas/{id}',function($id){
            $obj = Cuenca::find($id);
            if(empty($obj))
            {
                abort(404,'Recurso no encontrado');
            }
            return $obj;
        });
        Route::get('/api/cuencas/{idcuenca}/yacimientos',function($idcuenca){
            $obj = Cuenca::find($idcuenca);
            if(empty($obj))
            {
                abort(404,'Recurso no encontrado');
            }
            return new YacimientoCollection(
                $obj->Yacimientos()
                    ->orderBy('idareayacimiento')->paginate());
        });
    }
}
