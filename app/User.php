<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;
    protected $primaryKey = 'idusuario';
    protected $table = 'usuarios';

    const TYPE = 'usuario';
    static function getColumnsRelationship(){
      return [
          DB::raw("'" . User::TYPE . "' as type"),
          'idusuario as id',
          ];
    }



    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cuit'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [

    ];

    public function Empresa() {
        return $this->belongsTo(Empresa::class,'idempresa','idempresa')
            ->select(Empresa::getColumnsRelationship());

        //return $this->hasOne(Empresa::class,'idempresa','idempresa');
    }
}
