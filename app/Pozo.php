<?php

namespace App;

use App\Http\Resources\PozoCollection;
use App\Http\Resources\ProduccionCollection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class Pozo extends Model
{
    protected $table = 'pozos';
    protected $primaryKey = 'idpozo';
    const TYPE = 'pozo';

    static function getColumnsRelationship(){
        return [
            DB::raw("'" . get_called_class()::TYPE . "' as type"),
            'idpozo as id',
            'sigla',
            'codigodesesco'];
    }

    public function Cuenca()
    {
        return $this->HasOne('App\Cuenca','codigodesesco','idcuenca')
            ->select(Cuenca::getColumnsRelationship());
    }
    public function Yacimiento()
    {
        return $this->HasOne('App\Yacimiento','codigodesesco','idareayacimiento')
            ->select(Yacimiento::getColumnsRelationship());
    }

    public function Produccion()
    {
        return $this->hasMany('App\Produccion','idpozo','idpozo');
    }
    static function Routes()
    {

        Route::get('/api/pozos',function(){
            return new PozoCollection(
                Pozo::orderBy('idpozo')->paginate());
        });

        Route::get('/api/pozos/{id}',function($id){
            $obj = Pozo::find($id);
            if(empty($obj))
            {
                abort(404,'Recurso no encontrado');
            }
            return $obj;
        });

        Route::get('/api/pozos/{idpozo}/produccion',function($idpozo){
            $obj = Pozo::find($idpozo);
            if(empty($obj))
            {
                abort(404,'Recurso no encontrado');
            }
            return new ProduccionCollection(
                $obj->Produccion()
                    ->orderBy('anio','asc')
                    ->orderBy('mes','asc')
                    ->paginate());
        });
        Route::get('/api/pozos/{idpozo}/produccion/{anio}',function($idpozo,$anio){
            $obj = Pozo::find($idpozo);
            if(empty($obj))
            {
                abort(404,'Recurso no encontrado');
            }
            return new ProduccionCollection(
                $obj->Produccion()
                    ->where('anio',$anio)
                    ->orderBy('anio','asc')
                    ->orderBy('mes','asc')
                    ->paginate());
        });
    }
}
