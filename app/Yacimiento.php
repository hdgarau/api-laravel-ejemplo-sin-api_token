<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Yacimiento extends Model
{
    protected $table = 'areasyacimientos';
    protected $primaryKey = 'idareayacimiento';
    const TYPE = 'yacimiento';

    static function getColumnsRelationship(){
        return [
            DB::raw("'" . get_called_class()::TYPE . "' as type"),
            'idareayacimiento as id',
            'yacimiento as nombre',
            'codigodesesco'];
    }

    public function estadoArea(){
        return $this->hasOne(EstadoArea::class,'idestadodearea','idestadodearea');
    }

    public function Cuencas()
    {
        return $this->belongsToMany('App\Cuenca','areasyacimientoscuencas','idareayacimiento','idcuenca');

    }
}
